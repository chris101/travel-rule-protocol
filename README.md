Travel Rule Protocol (TRP)
=====================================

[The Travel Rule Protocol (TRP)](https://www.travelruleprotocol.org/) is a minimal, pragmatic API for compliance with FATF Travel Rule recommendations for virtual assets.

This repository contains notes from [past discussions](/discussions) and the a collection of protocol [extensions](/extensions). To suggest a new extension please join our working group through the [website](https://www.travelruleprotocol.org/).
